# README #

Use Qt5.6 and KF5 framework-solid for Solid::Device

### What is this repository for? ###

* show that multi mount points is not show, only the last one.
* mount a partition formated in ZFS and look if mount point is show (for me it is empty).

### what i see, in which situation ? ###

* if i have a mount point and i mount some bind type points, only the last mount will be show (that is not usefull)
* if there is a zfs partition, mount point is not see
* if there is an systemd-automount partition, it could be show only when you go in terminal an "ls /the/mount/point" for a time determined by the option for automount point on systemd-automount (maybe same with autofs used)

### How do I get set up? ###

* need to have KF5 Solid packages installed (i use solid-5.21 from archlinux)
* for ZFS, need zfs packages (contain kernel modules dkms, and zfs and zpool) and spl packages (contain kernel module and spl), after run the service with systemd, partitions are mounted automatically, and lsblk show them.

### Contribution guidelines ###

* tests
