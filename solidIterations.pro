#-------------------------------------------------
#
# Project created by QtCreator 2016-05-06T09:52:49
#
#-------------------------------------------------

QT       += core gui Solid

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = solidIterations
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
