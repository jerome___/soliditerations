#include "mainwindow.h"
#include <iostream>
#include <sstream>
#include <QTableView>
#include <QHeaderView>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent) {
    setupUi(this);
	disks_model = new QStandardItemModel;
	partitions_model = new QStandardItemModel;
	//disks_model = 0;
	//partitions_model = 0;
    refreshDiskArea();
}

MainWindow::~MainWindow() {
	partitions_model->deleteLater();
	disks_model->deleteLater();
	if (stv)
		delete stv;
}

//***  PRIVATE MEMBER FUNCTIONS  ***********************************************

void MainWindow::refreshDiskArea() {  // show storage devices connected
	disks_model->clear();
    storageDevices = Solid::Device::listFromType(
                                    Solid::DeviceInterface::StorageDrive,
                                    QString()); // all storage devices plugged
    foreach(Device disk, storageDevices) {
        QList<QStandardItem*> row;
        row << new QStandardItem(disk.description());
        row << new QStandardItem(disk.product());
        Solid::StorageDrive *std = disk.as<Solid::StorageDrive>();
        row << new QStandardItem(QString("%1").arg(std->size()));
        disks_model->insertRow(disks_model->rowCount(), row);
    }
    disks_model->setHeaderData(0, Qt::Horizontal, tr("Storage Device"));
    disks_model->setHeaderData(1, Qt::Horizontal, tr("Identity"));
    disks_model->setHeaderData(2, Qt::Horizontal, tr("Size"));
    disks->setModel(disks_model);
	disks->verticalHeader()->hide();
    disks->setSelectionMode(QAbstractItemView::SingleSelection);
    disks->setSelectionBehavior(QAbstractItemView::SelectRows);
    disks->horizontalHeader()->setStretchLastSection(true);
    disks->resizeColumnsToContents();
}

void MainWindow::refreshPartitionsArea() {
	partitions_model->clear();
    foreach(Device partition, storageVolumes) {
		QList<QStandardItem*> row;
        try {
            stv = partition.as<Solid::StorageVolume>(); }
        catch (...) {
            qDebug() << "ERREUR :";
        }
        QString formedType = (stv->fsType() == "zfs_member")
                           ? "zfs" : stv->fsType();
        QString formedMNT = (formedType == "zfs")
                          ? getZFSmountPointFor(stv->label())
                          : getMountPtFrom(partition.udi());
		row << new QStandardItem(stv->label())
            << new QStandardItem(stv->uuid())
            << new QStandardItem(formedType)
            << new QStandardItem(formedMNT);
        partitions_model->insertRow(partitions_model->rowCount(), row);
    }
    partitions_model->setHeaderData(0, Qt::Horizontal, tr("Label"));
    partitions_model->setHeaderData(1, Qt::Horizontal, tr("UUID"));
    partitions_model->setHeaderData(2, Qt::Horizontal, tr("Format"));
    partitions_model->setHeaderData(3, Qt::Horizontal, tr("mount point"));
	partitions->setModel(partitions_model);
    partitions->verticalHeader()->hide();
    partitions->setSelectionMode(QAbstractItemView::SingleSelection);
    partitions->setSelectionBehavior(QAbstractItemView::SelectRows);
    partitions->horizontalHeader()->setStretchLastSection(true);
    partitions->resizeColumnsToContents();
}

void MainWindow::washStorageSolidDirtyPartitions()
{ // wash the dirty partition datas who make:
  // empty entries,
  // doubles entries
  // and crash application if remove some dirty things
  // (crash when remove dirty things is maybe not from solid bugged code)
    QStringList labels;
    QList<Device>::iterator it = storageVolumes.begin();
    while(it++ != storageVolumes.end()) {
        std::stringstream ss; // sizeof will not show that data has something wrong
        ss << it.i->v; // so sstrem is for be able to read data length of void*
        qDebug() << "adress it.i->v = " << it.i->v
                 << "length:" << ss.str().size(); // at crash time, pointer should be null
                                                  // or an adress out of range that sizeof
                                                  // will never show
        // this happen for me when a disk has zfs partition and also btrfs partition inside.
        // i don't know how to include a usable bug report on solid KDE report
        // (and if someone care or want to see reports about bugs on solid)
        // but in case you are and know, maybe do it well has you want. (thank you for users community)
        if(it.i->v !=0 && ss.str().size() == 9) { // prevent crash from bugged solid
            Solid::StorageVolume *stv = (*it).as<Solid::StorageVolume>();
            if(stv->label().isEmpty() || !isUniq(labels, stv->label()))
                storageVolumes.erase(it);
            else
                labels << stv->label(); }
    }
}

bool MainWindow::isUniq(const QStringList &list,
                             const QString &element) {
    foreach(QString toCompare, list) {
        if(element == toCompare)
            return false;
    }
    return true;
}

QString MainWindow::getZFSmountPointFor(const QString &mntPT) {
    return QString("need to use libudev");
}

QString MainWindow::getMountPtFrom(const QString &udi) { // show mount pt
    QList<Device> strAccess = Device::listFromType(
                                Solid::DeviceInterface::StorageAccess,
                                udi);  // (i see only one mount point... why ?)
    QString mPoint(tr("not find"));
    foreach(Device access, strAccess) {
        if(access.as<Solid::StorageAccess>()) { // skip no StorageAccess
            Solid::StorageAccess *sta = access.as<Solid::StorageAccess>();
            mPoint = (mPoint == tr("not find"))
                        ? sta->filePath()
                        : mPoint + QString(", %1").arg(sta->filePath()); } }
    return mPoint;
}

//***  PRIVATE SOTS  ***********************************************************

void MainWindow::on_disks_clicked(const QModelIndex &index) {
    storageVolumes = Solid::Device::listFromType(  // only from selected Device
                                    Solid::DeviceInterface::StorageVolume,
                                    storageDevices.at(index.row()).udi());
    washStorageSolidDirtyPartitions();
    refreshPartitionsArea();
}

void MainWindow::on_pushButton_clicked() { exit(0); }
