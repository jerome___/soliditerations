#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QStandardItemModel>
#include <Solid/Device>
#include <Solid/DeviceInterface>
#include <Solid/StorageAccess>
#include <Solid/StorageDrive>
#include <Solid/StorageVolume>
#include <Solid/DeviceNotifier>

#include "ui_mainwindow.h"


class MainWindow : public QMainWindow,
                   private Ui::MainWindow,
                   private Solid::Device
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
    void on_disks_clicked(const QModelIndex &index);
    void on_pushButton_clicked();

private:
    QStandardItemModel     *disks_model, *partitions_model;
    QList<Device>           storageDevices, storageVolumes;
	Solid::StorageVolume *stv;
    void        refreshDiskArea();
    void        refreshPartitionsArea();
    void        washStorageSolidDirtyPartitions();
    bool        isUniq(const QStringList &list, const QString &element);
    QString     getMountPtFrom(const QString &udi);
    QString     getZFSmountPointFor(const QString &mntPT);
};

#endif // MAINWINDOW_H
